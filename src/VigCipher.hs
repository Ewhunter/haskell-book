module VigCipher(
    module VigCipher,
    module Data.Char
)
where
import Data.Char

lowerAlpha = ['a'..'z']
upperAlpha = ['A'..'Z']
alpha = upperAlpha ++ lowerAlpha

sample = "this is a SAMPLE string"

shiftInt :: Int -> Int -> Int -> Int
shiftInt range shiftAmount inputValue = calc
    where calc = mod (inputValue + actualShiftAmount) range
          actualShiftAmount = mod shiftAmount range

shiftChar :: Char -> Int -> Int -> Char -> Char
shiftChar baseChar range shiftAmount inputChar = chr $ shiftInt range shiftAmount currentValue + baseValue
    where currentValue = inputValue - baseValue
          inputValue = ord inputChar
          baseValue = ord baseChar

shiftWithAlpha :: String -> Int -> Char -> Char
shiftWithAlpha alphabet shiftAmount inputChar =
    shiftChar (head alphabet) (length alphabet) shiftAmount inputChar

simpleCaesarChar :: Char -> Int -> Char
simpleCaesarChar inputChar shiftAmount
    | inputChar `elem` lowerAlpha = shiftWithAlpha lowerAlpha shiftAmount inputChar
    | inputChar `elem` upperAlpha = shiftWithAlpha upperAlpha shiftAmount inputChar
    | otherwise = inputChar

simpleCaesar :: String -> Int -> String
simpleCaesar inputString shiftAmount = map (flip simpleCaesarChar shiftAmount) inputString

simpleUnCaesar :: String -> Int -> String
simpleUnCaesar inputString shiftAmount = simpleCaesar inputString (negate shiftAmount)

generateAlphaBasedShiftAmountForAlpha :: String -> Char -> Int
generateAlphaBasedShiftAmountForAlpha alpha shiftChar = offset
    where baseValue = ord $ head alpha
          shiftValue = ord shiftChar
          offset = shiftValue - baseValue

generateAlphaBasedShiftAmount :: Char -> Int
generateAlphaBasedShiftAmount shiftChar
    | shiftChar `elem` upperAlpha = generateAlphaBasedShiftAmountForAlpha upperAlpha shiftChar
    -- | shiftChar `elem` lowerAlpha = generateAlphaBasedShiftAmountForAlpha lowerAlpha shiftChar
    -- | shiftChar `elem` upperAlpha = generateAlphaBasedShiftAmountForAlpha upperAlpha shiftChar
    | otherwise = 0

-- looper accList inputString keyString =
--     case inputString of
--         (x:xs) -> looper (accList ++ [(x, cipherChar, shiftAmount, outChar)]) xs newKey
--             where isActive = x `elem` alpha
--                   newKey = if isActive then tail keyString else keyString
--                   cipherChar = if isActive then head keyString else ' '
--                   shiftAmount = if isActive then generateAlphaBasedShiftAmount cipherChar else 0
--                   outChar = if isActive then shiftWithAlpha upperAlpha shiftAmount x else x
--         "" -> accList
--
-- callLooper inputString keyString = looper [] inputString realKey
--     where realKey = keyString ++ realKey

buildVigenereMap inputString keyString = go [] inputString keyStringIter
    where keyStringIter = keyString ++ keyStringIter
          go accList inputString keyStringIter =
              case inputString of
                  (x:xs) -> go (accList ++ [(x, cipherChar, isActive)]) xs newKeyStringIter
                      where isActive = x `elem` upperAlpha
                            newKeyStringIter = if isActive then tail keyStringIter else keyStringIter
                            cipherChar = if isActive then head keyStringIter else ' '
                  "" -> accList

viginereCipher inputString keyString = map processMapEntry cipherMap
    where
        cipherMap = buildVigenereMap inputString keyString
        processMapEntry (x, cipherChar, True) = outChar
            where
                shiftAmount = generateAlphaBasedShiftAmount cipherChar
                outChar = shiftWithAlpha upperAlpha shiftAmount x
        processMapEntry (x, _, False) = x

ally = "ALLY" ++ ally
allyShort = take 20 ally

vigInput = "MEET AT DAWN meet at dawn 1234 !@#$"
vigOutput = viginereCipher vigInput "ALLY"
