module Chap7 where

myNum :: Integer
myNum = 2

myVal f = myNum
myVal2 f = f + myNum

other a b c = a ++ b ++ c

-- x = 1
-- y f = f + x

addOne :: Integer -> Integer
addOne x = let z = x + 1 in z

-- mTh1 x y z = x * y * z
-- mTh2 x y = \z -> x * y * z
-- mTh3 x = \y -> \z -> x * y * z
-- mTh4 = \x -> \y -> \z -> x * y * z

-- addOneIfOdd n = case odd n of
--     True -> f n
--     False -> n
--     where f n = n + 1
--
-- addOneIfOdd' = \n -> case odd n of
--     True -> f n
--     False -> n
--     where f n = n + 1

-- addFive x y = (if x > y then y else x) + 5
-- addFive' =  \x -> \y -> (if x > y then y else x) + 5
--
-- mflip f = \x -> \y -> f y x
-- mflip' f x y = f y x

isTwo x = case x of
    2 -> True
    _ -> False

newtype Username = Username String
newtype AccountNumber = AccountNumber Integer
data User = UnregisteredUser
    | RegisteredUser Username AccountNumber

printUser UnregisteredUser = putStrLn "UnregisteredUser"
printUser (RegisteredUser (Username name) (AccountNumber acctNum)) =
    putStrLn $ name ++ " " ++ show acctNum

data WherePengiunsLive =
    Galapagos
    | Antarctica
    | Australia
    | SouthAfrica
    | SouthAmerica
    deriving (Eq, Show)

data Penguin =
    Peng WherePengiunsLive
    deriving (Eq, Show)

isSouthAfrica SouthAfrica = True
isSouthAfrica _ = False

gimmeWhereTheyLive (Peng whereitlives) = whereitlives

humboldt = Peng SouthAmerica
gentoo = Peng Antarctica
macaroni = Peng Antarctica
little = Peng Australia
galapagos = Peng Galapagos

galapagosPenguin (Peng Galapagos) = True
galapagosPenguin _ = False

antarcticPenguin (Peng Antarctica) = True
antarcticPenguin _ = False

antarcticOrGalapagosPenguin p = antarcticPenguin p || galapagosPenguin p

-- f :: (a, b, c) -> (d, e, f) -> ((a, d), (c, f))
-- f (a, b, c) (d, e, f) = ((a, d), (c, f))

functionC x y = if x > y then x else y
-- functionC' x y = case x > y of
--     True -> x
--     False -> y

ifEvenAdd2 n = if even n then n+2 else n
-- ifEvenAdd2' n = case even n of
--     True -> n + 2
--     False -> n

nums x = case compare x 0 of
    LT -> -1
    EQ -> 0
    GT -> 1

data Employee = Coder
    | Manager
    | Veep
    | CEO
    deriving (Eq, Ord, Show)

reportBoss :: Employee -> Employee -> IO ()
reportBoss e e' =
    putStrLn $ show e ++ " is the boss of " ++ show e'

employeeRank :: Employee -> Employee -> IO ()
employeeRank e e' = case compare e e' of
    GT -> reportBoss e e'
    EQ -> putStrLn "Equal"
    LT -> reportBoss e' e

employeeRank' :: (Employee -> Employee -> Ordering) -> Employee -> Employee -> IO ()
employeeRank' f e e' = case f e e' of
    GT -> reportBoss e e'
    EQ -> putStrLn "Equal"
    LT -> reportBoss e' e

coderRule :: Employee -> Employee -> Ordering
coderRule Coder Coder = EQ
coderRule Coder _ = GT
coderRule _ Coder = LT
coderRule e e' = compare e e'

-- dodgy x y = x + y * 10
-- oneIsOne = dodgy 1
-- oneIsTwo = flip dodgy 2

myAbs x
    | x < 0 = -x
    | otherwise = x

-- Chapter Exercises
tensDigit :: Integral a => a -> a
tensDigit x = d
    where xLast = x `div` 10
          d = xLast `mod` 10

tensDigit' :: Integral a => a -> a
tensDigit' x = let divved = fst (divMod x 10)
    in snd (divMod divved 10)
  -- where xLast = x `div` 10
  --       d = xLast `mod` 10

hundredsDigit :: Integral a => a -> a
hundredsDigit x = d
    where xLast = x `div` 100
          d = xLast `mod` 10

thousandsDigit :: Integral a => a -> a
thousandsDigit x = d
    where xLast = x `div` 1000
          d = xLast `mod` 10

foldBool3 :: a -> a -> Bool -> a
foldBool3 x y True = x
foldBool3 x y False = y

foldBool4 :: a -> a -> Bool -> a
foldBool4 x y b
    | b = x
    | not b = y

gFill :: (a -> b) -> (a, c) -> (b, c)
gFill f (a, c) = (f a, c)

roundTrip :: (Show a, Read a) => a -> a
roundTrip a = read (show a)

roundTripPointfree :: (Show a, Read a) => a -> a
roundTripPointfree = read . show

roundTrip2 :: (Show a, Read b) => a -> b
roundTrip2 a = read (show a)

roundTripPointfree2 :: (Show a, Read b) => a -> b
roundTripPointfree2 = read . show

data SumOfThree a b c  =
    FirstPossible a
    | SecondPossible b
    | ThirdPossible c
    deriving (Eq, Show)

-- x = SumOfThree 'a' True 15
x = FirstPossible 'a'
y = SecondPossible True
yy = SecondPossible 4
yyy = SecondPossible 5
z = ThirdPossible 16

sumToInt :: SumOfThree a b c -> Integer
sumToInt (FirstPossible _) = 0
sumToInt _ = 1
