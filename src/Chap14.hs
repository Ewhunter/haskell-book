module Chap14
where

import Test.Hspec

dividedBy :: Integral a => a -> a -> (a, a)
dividedBy num denom = go num denom 0
    where go n d count
            | n < d = (count, n)
            | otherwise = go (n - d) d (count + 1)

additionTest :: IO ()
additionTest = hspec $
    describe "Addition" $
    do
        it "1 + 1 > 1" $
            (1 + 1) > 1 `shouldBe` True
        it "2 + 2 == 4" $
            (2 + 2) `shouldBe` 4

divisionTest :: IO ()
divisionTest = hspec $
    describe "Division" $
    do
        it "15 / 3 == 5" $
            dividedBy 15 3 `shouldBe` (5, 0)
        it "22 / 5 == 4 rem 2" $
            dividedBy 22 5 `shouldBe` (4, 2)
