module Chap12(
    module Chap12,
    module Chap11,
    module Data.Maybe
)
where
import Data.Maybe
import Data.Char
import Chap11

ifEvenAdd2 :: Integer -> Maybe Integer
ifEvenAdd2 n = if even n then Just (n + 2) else Nothing

type Name = String
type Age = Integer
data Person = Person Name Age deriving (Eq, Show)
type ValidatePerson a = Either [PersonInvalid] a

ageOK :: Age -> Either [PersonInvalid] Age
ageOK age = if age >= 0
    then Right age
    else Left [AgeTooLow]

nameOK :: Name -> Either [PersonInvalid] Name
nameOK name = if name /= ""
    then Right name
    else Left [NameEmpty]

mkPerson :: Name -> Age -> Maybe Person
mkPerson name age
    | name /= "" && age >= 0 = Just $ Person name age
    | otherwise = Nothing

data PersonInvalid = NameEmpty | AgeTooLow deriving (Eq, Show)

mkPerson2 :: Name -> Age -> Either PersonInvalid Person
mkPerson2 name age
    | name /= "" && age >= 0 = Right $ Person name age
    | name == "" = Left NameEmpty
    | otherwise = Left AgeTooLow

mkValidatedPerson :: Name -> Age -> ValidatePerson Person
mkValidatedPerson name age = go (nameOK name) (ageOK age)
    where
        go :: ValidatePerson Name -> ValidatePerson Age -> ValidatePerson Person
        go (Right nOK) (Right aOK) = Right (Person nOK aOK)
        go (Left nBad) (Left aBad) = Left (nBad ++ aBad)
        go (Left nBad) _ = Left nBad
        go _ (Left aBad) = Left aBad

safeTail :: [a] -> Maybe [a]
safeTail [] = Nothing
safeTail [_] = Nothing
safeTail (_:xs) = Just xs

-- Chapter Exercises
notThe :: String -> Maybe String
notThe s = if s == "the" then Just "a" else Nothing

testString = "the really cool the AWESOME test"
testString2 = "the evil THE COW THE OTHER"
testStringBig = unwords [testString, testString2]
replaceThe :: String -> String
replaceThe = unwords . map substituteThe . words
    where
        substituteThe word = if word == "the" then "a" else word

lowercaseString :: String -> String
lowercaseString = map toLower

countTheBeforeVowel :: String -> Integer
countTheBeforeVowel s = let
    vowels = "aeiou"
    input = words $ lowercaseString s

    isThe :: String -> Bool
    isThe = ("the" ==)

    startsWithVowel :: String -> Bool
    startsWithVowel "" = False
    startsWithVowel (x:xs) = elem x vowels

    foundMatch :: Bool -> String -> Bool
    foundMatch prevThe word = prevThe && startsWithVowel word

    computeNewCount :: Integer -> Bool -> String -> Integer
    computeNewCount count prevThe word = if foundMatch prevThe word then count + 1 else count

    run :: Integer -> Bool -> String -> [String] -> Integer
    run count prevThe currentWord [] = computeNewCount count prevThe currentWord
    run count prevThe currentWord (word:words) = run (computeNewCount count prevThe currentWord) (isThe currentWord) word words

    in run 0 False "" input

vowels = "aeiou"
consonants = stringFilter True vowels ['a'..'z']

stringFilter :: Bool -> String -> String -> String
stringFilter negateFilter filterString = filter filterFunc
    where
        baseFilter = (`elem` filterString)
        filterFunc = if negateFilter then not . baseFilter else baseFilter

countStringFilter :: String -> String -> Int
countStringFilter filterString = length . stringFilter False filterString . lowercaseString

countVowels :: String -> Int
countVowels = countStringFilter vowels

newtype Word' = Word' String deriving (Eq, Show)

mkWord :: String -> Maybe Word'
mkWord s = let
    numVowels = countVowels s
    numConsonants = countStringFilter consonants s
    in if numVowels > numConsonants then Nothing else Just $ Word' s

data Nat = Zero | Succ Nat deriving (Eq, Show)

natToInteger :: Nat -> Integer
natToInteger = go 0 where
    go count Zero = count
    go count (Succ x)= go (count+1) x

integerToNat :: Integer -> Maybe Nat
integerToNat input = if input < 0 then Nothing else Just $ go input Zero
    where
        go 0 value = value
        go n value = go (n - 1) $ Succ value

integerToNat' :: Integer -> Maybe Nat
integerToNat' n = if n < 0 then Nothing else Just $ foldr (\_ x -> Succ x) Zero [1..n]

-- Maybe Library
isJust' :: Maybe a -> Bool
isJust' Nothing = False
isJust' _ = True

isNothing' :: Maybe a -> Bool
isNothing' = not . isJust'

mayybe :: b -> (a -> b) -> Maybe a -> b
mayybe x f Nothing = x
mayybe x f (Just a) = f a

fromMaybe' :: a -> Maybe a -> a
fromMaybe' defaultValue = mayybe defaultValue id

listToMaybe' :: [a] -> Maybe a
listToMaybe' [] = Nothing
listToMaybe' (x:xs) = Just x

maybeToList' :: Maybe a -> [a]
maybeToList' Nothing = []
maybeToList' (Just x) = [x]

catMaybes' :: [Maybe a] -> [a]
catMaybes' = foldr process []
    where
        process Nothing acc = acc
        process (Just x) acc = x : acc

flipMaybe :: [Maybe a] -> Maybe [a]
flipMaybe [] = Just []
flipMaybe (Nothing:xs) = Nothing
flipMaybe (Just x:xs) = let
    remainderResult = flipMaybe xs
    in
        if isNothing' remainderResult
        then Nothing
        else Just (x:fromJust remainderResult)

-- Either Library

eithers = [Left 15, Left 16, Right 14, Right 17, Left 18, Right 20]

lefts' :: [Either a b] -> [a]
lefts' = foldr process [] where
    process (Left x) acc = x:acc
    process _ acc = acc

rights' :: [Either a b] -> [b]
rights' = foldr process [] where
    process (Right x) acc = x:acc
    process _ acc = acc

partitionEithers' :: [Either a b] -> ([a], [b])
partitionEithers' = foldr process ([], []) where
    process (Left a) (as, bs) = (a:as, bs)
    process (Right b) (as, bs) = (as, b:bs)

eitherMaybe' :: (b -> c) -> Either a b -> Maybe c
eitherMaybe' f eitherVal = case eitherVal of
    Left a -> Nothing
    Right b -> Just $ f b

either' :: (a -> c) -> (b -> c) -> Either a b -> c
either' fA fB eitherVal = case eitherVal of
    Left a -> fA a
    Right b -> fB b

eitherMaybe'' :: (b -> c) -> Either a b -> Maybe c
eitherMaybe'' f = either (const Nothing) (Just . f)

stringToInt :: String -> Int
stringToInt _ = 15

doubleToInt :: Double -> Int
doubleToInt _ = 14

l = Left "thing"
r = Right (6 :: Integer)

dd :: (Num a) => a -> a
dd = (2*)

myIterate :: (a -> a) -> a -> [a]
myIterate f a = a : myIterate f (f a)

myUnfoldr :: (b -> Maybe (a, b)) -> b -> [a]
myUnfoldr f = go where
    -- go :: b -> [a] -> [a]
    go startVal = case f startVal of
        Nothing -> []
        Just (x, y) -> x : go y

betterIterate :: (a -> a) -> a -> [a]
betterIterate f = myUnfoldr (\x -> Just (x, f x))

testFolderBuilder :: (Ord b, Num b) => b -> b -> Maybe (b, b)
testFolderBuilder limit b = if b >= limit then Nothing else Just (b, b+1)

treeFolder :: (Ord b, Num b) => b -> (b -> Maybe (b, b, b))
treeFolder limit x = if x > limit then Nothing else Just (1+x, x, 1+x)

treeFolderWeird :: (Ord b, Num b) => b -> (b -> Maybe (b, b, b))
treeFolderWeird limit x = if x > limit then Nothing else Just (1+x, x, 2+x)

newTree :: a -> BinaryTree a
newTree x = Node Leaf x Leaf

unfoldTree :: (a -> Maybe (a, b, a)) -> a -> BinaryTree b
unfoldTree f = go where
    go startVal = case f startVal of
        Nothing -> Leaf
        Just (x, y, z) -> Node (go x) y (go z)

buildTree :: Integer -> BinaryTree Integer
buildTree n = unfoldTree (treeFolder n) 0

buildTree' :: Integer -> BinaryTree Integer
buildTree' n = foldr (\x y -> Node y x y) Leaf [0..(n-1)]
