module Jammin(
    module Jammin,
    module Data.List
)
where

import Data.List

data Fruit =
    Peach
    | Plum
    | Apple
    | Blackberry
    deriving (Eq, Ord, Show)

data JamJars =
    Jam {
        fruit :: Fruit,
        jars :: Int
    }
    deriving (Eq, Ord, Show)

row1 = Jam Peach 3
row2 = Jam Plum 4
row3 = Jam Apple 10
row4 = Jam Blackberry 4
row5 = Jam Peach 5
row6 = Jam Plum 8
row7 = Jam Blackberry 7
allJam = [row1, row2, row3, row4, row5, row6, row7]

totalJarCount = sum $ map jars allJam

compJamJars (Jam _ x) (Jam _ y) = compare x y
compJamKind (Jam x _) (Jam y _) = compare x y

sameKind (Jam x _) (Jam y _) = x == y

groups = groupBy sameKind $ sort allJam
