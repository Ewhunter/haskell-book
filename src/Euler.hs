module Euler
where
import Data.Set hiding (map, filter, foldr)

divisors :: Int -> [Int]
divisors n = [x | x <- [1..z], mod n x == 0]
    where z = (ceiling . sqrt . fromIntegral) (n-1)

divisors2 :: Int -> [(Int, Int)]
divisors2 n = [(x, div n x) | x <- [1..z], mod n x == 0]
    where z = (ceiling . sqrt . fromIntegral) (n-1)

properDivisors n = delete n $ fromList $ concatMap lister $ divisors2 n

dat :: Int -> [(Int, Int)]
dat n = [(x, sum $ properDivisors x) | x <- [0..n]]

filtered :: Int -> [(Int, Int)]
filtered n = [(a, b) | (a, b) <- dd, a /= b && a < n && b < n && a == snd (dd!!b) && b == fst (dd!!b)]
    where
        dd = dat n

lister :: (Int, Int) -> [Int]
lister (a, b) = if a == b then [a] else [a, b]

euler21 :: Int
euler21 = sum $fromList $concatMap lister$ filtered  10000

-- sieve (p:xs) = p : sieve [x | x <- xs, mod x p > 0]
sieve (p:xs) = p : sieve [x | x <- xs, mod x p > 0]
primes' = sieve [2..]
